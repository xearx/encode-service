#!/bin/sh

mkdir encoder_pb &> /dev/null
protoc -I../protos/ \
    ../protos/encoder.proto \
    --go_out=plugins=grpc:. \
    --go_opt=module=$(head -n 1 ./go.mod | awk '{ print $2 }') \
    --plugin=protoc-gen-go=$GOPATH/bin/protoc-gen-go

#!/bin/sh

go get github.com/golang/protobuf/protoc-gen-go
go mod tidy
go mod verify

package main

import (
	"bytes"
	"context"
	"crypto/sha512"
	"fmt"
	"log"
	"net"

	"gitlab.com/the-xeptore/xearx/encode-service/config"
	pb "gitlab.com/the-xeptore/xearx/encode-service/encoder_pb"
	"google.golang.org/grpc"
)

type encodeServer struct{}

func (s *encodeServer) EncodePages(ctx context.Context, request *pb.PageContents) (*pb.EncodedPages, error) {
	reply := pb.EncodedPages{EncodedPageContents: make(map[string]string, len(request.PageContents))}
	for key, content := range request.PageContents {
		buf := bytes.Buffer{}
		buf.WriteString(content)
		sum := sha512.Sum512(buf.Bytes())
		str := fmt.Sprintf("%x", sum)
		reply.EncodedPageContents[key] = str
	}
	return &reply, nil
}

func newServer() *encodeServer {
	s := &encodeServer{}
	return s
}

func main() {
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterEncoderServer(grpcServer, newServer())
	cfg := config.Init()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", cfg.Grpc.ServerPort))
	if nil != err {
		log.Fatalf("error initializing server: %s", err)
	}
	grpcServer.Serve(lis)
}

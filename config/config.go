package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// Config represents typed configuration.
type Config struct {
	Grpc struct {
		ServerPort int `mapstructure:"server_port"`
	} `mapstructure:"grpc"`
}

// Init intialize a configuration instance.
func Init() *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("fatal error config file %s", err))
	}
	cfg := &Config{}
	viper.Unmarshal(&cfg)
	return cfg
}
